import asyncio
import discord
from discord.ext import commands
from discord.ext.commands import Cog


class Common(Cog):
    def __init__(self, bot):
        self.bot = bot
        self.bot.remove_role_if_no_users = self.remove_role_if_no_users
        self.bot.rolebot_check_limit = self.rolebot_check_limit
        self.bot.rolebot_guild_colors = self.rolebot_guild_colors
        self.bot.get_role = self.get_role
        self.bot.remove_all_rolebot_color_roles = self.remove_all_rolebot_color_roles
        self.bot.user_pronouns = self.user_pronouns
        self.bot.async_call_shell = self.async_call_shell
        self.bot.slice_message = self.slice_message
        self.bot.clear_roles = self.clear_roles
        self.max_split_length = 3
        self.bot.remove_all_user_pronoun_roles = self.remove_all_user_pronoun_roles
        self.bot.owner_or_manage_roles = self.owner_or_manage_roles
        self.bot.move_roles = self.move_roles
        self.bot.get_rolebot_role = self.get_rolebot_role
        self.bot.query_setting = self.query_setting
        self.bot.setting_overriden = self.setting_overriden
        self.bot.query_pronoun = self.query_pronoun
        self.bot.check_color = self.check_color

    def setting_overriden(self, setting, guild_id):
        """Checks if an override is set for the setting in question"""
        guild_id = str(guild_id)
        return (guild_id in self.bot.config["overrides"]) and (
            setting in self.bot.config["overrides"][guild_id]
        )

    def query_setting(self, setting, guild_id=""):
        """Query a guild's setting from config file

        reverts to default if no override is set"""
        guild_id = str(guild_id)
        if guild_id and self.setting_overriden(setting, guild_id):
            return self.bot.config["overrides"][guild_id][setting]
        return self.bot.config["defaults"][setting]

    def query_pronoun(self, userstr, guild_id=""):
        pronouns = self.query_setting("pronouns", guild_id)
        userstr = userstr.lower().strip()
        for pronoun in pronouns:
            if pronoun.lower().startswith(userstr):
                return pronoun
        return None

    # 2000 is maximum limit of discord
    async def slice_message(self, text, size=2000, prefix="", suffix=""):
        """Slices a message into multiple messages"""
        if len(text) > size * self.max_split_length:
            haste_url = await self.haste(text)
            return [
                f"Message is too long ({len(text)} > "
                f"{size * self.max_split_length} "
                f"({size} * {self.max_split_length}))"
                f", go to haste: <{haste_url}>"
            ]
        reply_list = []
        size_wo_fix = size - len(prefix) - len(suffix)
        while len(text) > size_wo_fix:
            reply_list.append(f"{prefix}{text[:size_wo_fix]}{suffix}")
            text = text[size_wo_fix:]
        reply_list.append(f"{prefix}{text}{suffix}")
        return reply_list

    async def haste(self, text, instance="https://hastebin.com/"):
        response = await self.bot.aiosession.post(f"{instance}documents", data=text)
        if response.status == 200:
            result_json = await response.json()
            return f"{instance}{result_json['key']}"

    async def async_call_shell(
        self, shell_command: str, inc_stdout=True, inc_stderr=True
    ):
        pipe = asyncio.subprocess.PIPE
        proc = await asyncio.create_subprocess_shell(
            str(shell_command), stdout=pipe, stderr=pipe
        )

        if not (inc_stdout or inc_stderr):
            return "??? you set both stdout and stderr to False????"

        proc_result = await proc.communicate()
        stdout_str = proc_result[0].decode("utf-8").strip()
        stderr_str = proc_result[1].decode("utf-8").strip()

        if inc_stdout and not inc_stderr:
            return stdout_str
        elif inc_stderr and not inc_stdout:
            return stderr_str

        if stdout_str and stderr_str:
            return f"stdout:\n\n{stdout_str}\n\n" f"======\n\nstderr:\n\n{stderr_str}"
        elif stdout_str:
            return f"stdout:\n\n{stdout_str}"
        elif stderr_str:
            return f"stderr:\n\n{stderr_str}"

        return "No output."

    async def owner_or_manage_roles(self, ctx):
        return (
            ctx.author == ctx.bot.app_info.owner
            or ctx.author.guild_permissions.manage_roles
        )

    async def move_roles(self, ctx):
        """Moves roles right under its own role, this is the backend."""
        pronouns = self.query_setting("pronouns", ctx.guild.id)
        for role in ctx.guild.roles:
            if role.name in pronouns or role.name.startswith("rolebot-"):
                our_role = await self.get_rolebot_role(ctx)
                our_pos = our_role.position
                new_pos = our_pos - 1
                new_pos = 1 if new_pos <= 0 else new_pos
                await role.edit(position=new_pos)
        return True

    async def clear_roles(self, ctx, dryrun=False):
        """Clears roles, this is the backend."""
        roles_deleted = 0
        pronouns = self.query_setting("pronouns", ctx.guild.id)
        for role in ctx.guild.roles:
            if role.name in pronouns or role.name.startswith("rolebot-"):
                if dryrun:
                    roles_deleted += 0 if role.members else 1
                else:
                    remove_result = await self.bot.remove_role_if_no_users(role)
                    roles_deleted += 1 if remove_result else 0
        return roles_deleted

    def check_color(self, guild_id, color_hex):
        allowedcolors = self.bot.query_setting("colors", guild_id)

        if not allowedcolors:
            return "Colors are not enabled in this guild."

        allow_set = (
            allowedcolors is True
            or (color_hex in allowedcolors)
            or color_hex.replace("#", "") in allowedcolors
        )

        if not allow_set:
            return "This color isn't whitelisted in this guild."

    def user_pronouns(self, user: discord.Member):
        """Returns the list of pronouns of a Member."""
        user_pronouns = []
        pronouns = self.query_setting("pronouns", user.guild.id)
        for role in user.roles:
            if role.name in pronouns:
                user_pronouns.append(role)
        return user_pronouns

    async def remove_role_if_no_users(self, role: discord.Role):
        if not role.members:
            await role.delete()
            return True
        return False

    def rolebot_check_limit(self, ctx) -> bool:
        """Checks if rolebot limit is used up in the guild"""
        rolebot_roles = 0
        for role in ctx.guild.roles:
            if role.name.startswith("rolebot-"):
                rolebot_roles += 1
        return rolebot_roles >= self.query_setting("maxroles", ctx.guild.id)

    def rolebot_guild_colors(self, ctx):
        """Returns the list of colors in the guild."""
        guild_colors = []
        for role in ctx.guild.roles:
            if role.name.startswith("rolebot-#"):
                guild_colors.append(role.name.split("-")[-1])
        return guild_colors

    # fuck my life coding at 4am
    async def get_rolebot_role(self, ctx):
        rolebot_role = discord.utils.get(ctx.guild.roles, name="RoleBot")
        if rolebot_role and ctx.me in rolebot_role.members:
            return rolebot_role
        elif rolebot_role:
            await ctx.me.add_roles(rolebot_role)
        else:
            rolebot_role = await ctx.guild.create_role(name="RoleBot")
        await ctx.me.add_roles(rolebot_role)
        return rolebot_role

    async def get_role(
        self,
        ctx,
        role_name: str,
        check_limit: bool = True,
        create_role: bool = True,
        role_color=None,
    ):
        """Gets a role with name, if it can't find it, creates the role"""
        role_with_name = discord.utils.get(ctx.guild.roles, name=role_name)
        if role_with_name:
            return role_with_name

        # if we're at this point then the role doesn't exist
        if check_limit and self.rolebot_check_limit(ctx):
            return None
        if create_role:
            our_role = await self.get_rolebot_role(ctx)
            our_pos = our_role.position
            # Can we improve this?
            new_role = None
            if role_color:
                new_role = await ctx.guild.create_role(
                    name=role_name, colour=role_color
                )
            else:
                new_role = await ctx.guild.create_role(name=role_name)
            await new_role.edit(position=our_pos)
            return new_role
        return None

    async def remove_all_rolebot_color_roles(
        self, user: discord.User, delete_unused: bool = True
    ):
        """Removes all rolebot color roles from a user"""
        # Make this less hacky
        for role in user.roles:
            if role.name.startswith("rolebot-#"):
                await user.remove_roles(role)
                if delete_unused:
                    await self.remove_role_if_no_users(role)

    async def remove_all_user_pronoun_roles(
        self, user: discord.Member, delete_unused: bool = True
    ):
        """Removes all rolebot color roles from a user"""
        # Make this less hacky
        pronouns = self.query_setting("pronouns", user.guild.id)
        for pronoun in pronouns:
            role_with_name = discord.utils.get(user.roles, name=pronoun)
            if role_with_name:
                await user.remove_roles(role_with_name)
                if delete_unused:
                    await self.bot.remove_role_if_no_users(role_with_name)


def setup(bot):
    bot.add_cog(Common(bot))
